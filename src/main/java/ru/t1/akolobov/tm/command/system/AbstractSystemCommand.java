package ru.t1.akolobov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.api.service.ICommandService;
import ru.t1.akolobov.tm.command.AbstractCommand;
import ru.t1.akolobov.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @Override
    @Nullable
    public Role[] getRoles() {
        return null;
    }

}
