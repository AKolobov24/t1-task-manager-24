package ru.t1.akolobov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.enumerated.Role;
import ru.t1.akolobov.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "user-change-password";

    @NotNull
    public static final String DESCRIPTION = "Change current user password.";

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE USER PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();
        @NotNull final String userId = getAuthService().getUserId();
        getUserService().setPassword(userId, password);
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return Role.values();
    }

}
