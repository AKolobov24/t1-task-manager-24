package ru.t1.akolobov.tm.util;

import org.jetbrains.annotations.NotNull;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

public interface HashUtil {

    String SECRET = "1324";

    Integer ITERATIONS = 4123;

    @NotNull
    static String salt(@NotNull final String value) {
        @NotNull String result = value;
        for (int i = 0; i < ITERATIONS; i++) {
            result = sha256(SECRET + result + SECRET);
        }
        return result;

    }

    @NotNull
    static String sha256(@NotNull final String value) {
        try {
            @NotNull final MessageDigest digest = MessageDigest.getInstance("SHA-256");
            final byte[] hash = digest.digest(value.getBytes(StandardCharsets.UTF_8));
            @NotNull final StringBuilder hexString = new StringBuilder();
            for (byte b : hash) {
                @NotNull final String hex = Integer.toHexString(0xff & b);
                if (hex.length() == 1)
                    hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

}
